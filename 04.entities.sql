USE [etrainu_ipa]

ALTER TABLE dbo.entities ADD
	entCanCollectBaselines bit NOT NULL CONSTRAINT DF_entities_entCanCollectBaselines DEFAULT 0
GO

INSERT INTO tbl_schemaVersion (alterScriptName) VALUES ( '04.entities.sql' )
GO