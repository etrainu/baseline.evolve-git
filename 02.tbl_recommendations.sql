USE [etrainu_ipa]

ALTER TABLE [dbo].[recommendations]
ALTER COLUMN recCourseTitle varchar(255)
GO

ALTER TABLE [dbo].[recommendations]
	ADD
		recLinkText varchar(255)
		,recLinkUrl varchar(255)
GO

ALTER TABLE recommendationMetricLink
	ADD 
		rmlValueFloor INT
		,rmlValueCeiling INT
GO

INSERT INTO tbl_schemaVersion (alterScriptName) VALUES ( '02.recommendations.sql' )
GO