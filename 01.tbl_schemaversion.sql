USE [etrainu_ipa]
GO

IF EXISTS (SELECT 1 
    FROM INFORMATION_SCHEMA.TABLES 
    WHERE TABLE_TYPE='BASE TABLE' 
    AND TABLE_NAME='tbl_schemaVersion')
        PRINT 'Table Already Exists!' 
ELSE BEGIN
	CREATE TABLE [dbo].[tbl_schemaVersion](
		[alterScriptName] [varchar](150) NOT NULL,
		[dateExecuted] [datetime] NOT NULL DEFAULT getdate()
	) ON [PRIMARY]
	
	INSERT INTO tbl_schemaVersion (alterScriptName) VALUES ( '01.tbl_schemaversion.sql' )
END

SET ANSI_PADDING OFF
GO