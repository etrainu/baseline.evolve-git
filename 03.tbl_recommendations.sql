USE [etrainu_ipa]

ALTER TABLE [dbo].[recommendations]
	ADD
		recThumbnail varchar(255)
GO

INSERT INTO tbl_schemaVersion (alterScriptName) VALUES ( '03.recommendations.sql' )
GO